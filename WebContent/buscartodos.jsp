<%@ page import="br.livros.entidade.Livros"%>
<%@ page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Mostrar todos os livros</title>
</head>
<body>
	
	<h1>Mostrar Todos os Livros</h1>
	
	<script type="text/javascript">
		function Nova()
		{
			location.href="cadastro.html"
		}
	</script>
	
	<input type="button" value="Cadastrar Novo Livro" onClick="Nova()"><br><br>
	
		
	<table border=1 cellspacing="0" width="70%">
	
		<tr bgcolor="black" style=color:white>
			<th>C�digo</th>
			<th>Livro</th>
			<th>Autor</th>
			<th>Editora</th>
			<th colspan="2">A��o</th>
		</tr>
		
		<%
			List<Livros> lista = (List<Livros>)request.getAttribute("listaLivros");
			for(Livros liv:lista) {
		%>
		
		<tr>
			<td><%= liv.getCodigo() %></td>
			<td><%= liv.getNome() %></td>
			<td><%= liv.getAutor() %></td>
			<td><%= liv.getEditora() %></td>
			<td>
				<a href="excluirlivro.do?codigo=<%= liv.getCodigo() %>">Excluir</a>
			</td>
			<td>
				<a href="obterdados.do?codigo=<%= liv.getCodigo() %>">Alterar</a>
			</td>
		</tr>
		
		<%
			}
		%>
	
	</table>
	
</body>
</html>