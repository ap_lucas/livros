package br.livros.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.livros.dao.LivrosDao;
import br.livros.entidade.Livros;

@WebServlet("/buscartodos.do")
public class BuscarTodos extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		LivrosDao livrosDao = new LivrosDao();
		List<Livros> listaLivros = livrosDao.buscarTodos();
		
		request.setAttribute("listaLivros", listaLivros);
		request.getRequestDispatcher("buscartodos.jsp").forward(request, response);
		
	}

}
