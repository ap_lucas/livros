package br.livros.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.livros.dao.LivrosDao;
import br.livros.entidade.Livros;

@WebServlet("/cadastro.do")
public class Cadastro extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String nome = request.getParameter("txtlivro");
		String autor = request.getParameter("txtautor");
		String editora = request.getParameter("txteditora");
		
		Livros livros = new Livros();
		
		livros.setNome(nome);
		livros.setAutor(autor);
		livros.setEditora(editora);
		
		LivrosDao livrosDao = new LivrosDao();
		livrosDao.cadastrar(livros);
		
		response.sendRedirect("http://localhost:7575/Livros/cadastro.html");
	}

}
