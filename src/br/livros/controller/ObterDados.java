package br.livros.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.livros.dao.LivrosDao;
import br.livros.entidade.Livros;

@WebServlet("/obterdados.do")
public class ObterDados extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Integer codigo = Integer.parseInt(request.getParameter("codigo"));
		
		LivrosDao livrosDao = new LivrosDao();
		Livros livros = livrosDao.buscarPorCodigo(codigo);
		livros.setCodigo(codigo);
		
		request.setAttribute("livros", livros);
		request.getRequestDispatcher("obterdados.jsp").forward(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Integer codigo = Integer.parseInt(request.getParameter("txtcodigo"));
		String nome = request.getParameter("txtlivro");
		String autor = request.getParameter("txtautor");
		String editora = request.getParameter("txteditora");
		
		Livros livros = new Livros();
		livros.setCodigo(codigo);
		livros.setNome(nome);
		livros.setAutor(autor);
		livros.setEditora(editora);
		
		LivrosDao livrosDao = new LivrosDao();
		livrosDao.alterar(livros);
		
		response.sendRedirect("http://localhost:7575/Livros/buscartodos.do");
		
	}

}
