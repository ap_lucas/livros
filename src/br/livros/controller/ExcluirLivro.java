package br.livros.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.livros.dao.LivrosDao;
import br.livros.entidade.Livros;

@WebServlet("/excluirlivro.do")
public class ExcluirLivro extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Integer codigo = Integer.parseInt(request.getParameter("codigo"));
		
		Livros livros = new Livros();
		livros.setCodigo(codigo);

		LivrosDao livrosDao = new LivrosDao();
		livrosDao.excluir(livros);

		response.sendRedirect("http://localhost:7575/Livros/buscartodos.do");
		
	}

}
