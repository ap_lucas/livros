package br.livros.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.livros.entidade.Livros;
import br.livros.jdbc.Conexao;


public class LivrosDao {

	public void cadastrar(Livros liv) {
		
		Connection con = Conexao.receberConexao();
		
		String sql = "INSERT INTO tb_livros (nome, autor, editora) VALUES (?, ?, ?)";
		
		try {
			PreparedStatement preparador = con.prepareStatement(sql);
			preparador.setString(1, liv.getNome());
			preparador.setString(2, liv.getAutor());
			preparador.setString(3, liv.getEditora());
			
			preparador.execute();
			
			preparador.close();
			
			System.out.println("Livro cadastrado com sucesso!");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	public void alterar(Livros liv) {
		
		Connection con = Conexao.receberConexao();
		
		String sql = "UPDATE tb_livros SET nome = ?, autor = ?, editora = ? WHERE codigo =?";
		
		try {
			PreparedStatement preparador = con.prepareStatement(sql);
			preparador.setString(1, liv.getNome());
			preparador.setString(2, liv.getAutor());
			preparador.setString(3, liv.getEditora());
			preparador.setInt(4, liv.getCodigo());
			
			preparador.execute();
			
			preparador.close();
			
			System.out.println("Livro alterado com sucesso!");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	public void excluir(Livros liv) {
		
		Connection con = Conexao.receberConexao();
		
		String sql = "DELETE FROM tb_livros WHERE codigo = ?";
		
		try {
			PreparedStatement preparador = con.prepareStatement(sql);
			preparador.setInt(1, liv.getCodigo());
			preparador.execute();
			preparador.close();
			
			System.out.println("Livro exclu�do com sucesso!");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	public Livros buscarPorCodigo(Integer codigo) {
		
		Connection con = Conexao.receberConexao();
		
		String sql = "SELECT * FROM tb_livros WHERE codigo = ?";
		
		Livros livros = null;
		
		try {
			PreparedStatement preparador = con.prepareStatement(sql);
			preparador.setInt(1, codigo);
			
			ResultSet resultado = preparador.executeQuery();
			
			if(resultado.next()) {
				livros = new Livros();
				livros.setNome(resultado.getString("nome"));
				livros.setAutor(resultado.getString("autor"));
				livros.setEditora(resultado.getString("editora"));
			}
			
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return livros;
	}
	
	public List<Livros> buscarTodos() {
		
		Connection con = Conexao.receberConexao();
		
		String sql = "SELECT * FROM tb_livros";
		
		ArrayList<Livros> listaLivros = new ArrayList<Livros>();
		
		try {
			PreparedStatement preparador = con.prepareStatement(sql);
			
			ResultSet resultado = preparador.executeQuery();
			
			while (resultado.next()) {
				Livros livros = new Livros();
				livros.setCodigo(resultado.getInt("codigo"));
				livros.setNome(resultado.getString("nome"));
				livros.setAutor(resultado.getString("autor"));
				livros.setEditora(resultado.getString("editora"));
				listaLivros.add(livros);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return listaLivros;
	}
	
}
