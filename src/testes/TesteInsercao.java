package testes;

import br.livros.dao.LivrosDao;
import br.livros.entidade.Livros;

public class TesteInsercao {

	public static void main(String[] args) {
		
		Livros livro = new Livros();
		
		livro.setNome("Teste");
		livro.setAutor("jk");
		livro.setEditora("1900");
		
		LivrosDao livrosDao = new LivrosDao();
		livrosDao.cadastrar(livro);
		
	}

}
