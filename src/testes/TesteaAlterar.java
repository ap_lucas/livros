package testes;

import br.livros.dao.LivrosDao;
import br.livros.entidade.Livros;

public class TesteaAlterar {

	public static void main(String[] args) {
	
		Livros livros = new Livros();
		
		livros.setNome("espa�o");
		livros.setAutor("tyson degresse");
		livros.setEditora("rocco");
		livros.setCodigo(8);
		
		LivrosDao livrosDao = new LivrosDao();
		
		livrosDao.alterar(livros);

	}

}
